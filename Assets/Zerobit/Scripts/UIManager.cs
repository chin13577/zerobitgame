﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    [SerializeField] GameObject tutorial;
    [SerializeField] GameObject menuPanel;
    [SerializeField] GameObject endPanel;
    [SerializeField] PlayerUI playerUI;
    [SerializeField] GameObject scorePanel;

    public void ActiveEndingPanel(bool isActive)
    {
        endPanel.SetActive(isActive);
    }
    public void ActiveTutorialPanel(bool isActive)
    {
        tutorial.SetActive(isActive);
    }
    public void ActiveScorePanel(bool isActive)
    {
        scorePanel.SetActive(isActive);
    }
    public void ActiveMenu(bool isActive)
    {
        menuPanel.SetActive(isActive);
    }
    public void ActivePlayerPanel(bool isActive)
    {
        if (isActive)
        {
            playerUI.Show();
        }
        else
        { 
            playerUI.Hide();
        }
    }

}
