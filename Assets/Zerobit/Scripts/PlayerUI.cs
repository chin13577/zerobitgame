﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] GameObject PlayerPanel;
    [SerializeField] Text heroDetailText;
    [SerializeField] Text enemyDetailText;

    HeroManager heroManager;
    private void Awake()
    {
        heroManager = GameManager.Instance.heroManager;
    }
    private void OnEnable()
    {
        HeroManager.OnSwapHero += HeroManager_OnSwapHero;
        Character.OnHeathChange += Character_OnReceiveDamage;
    }

    private void OnDisable()
    {
        HeroManager.OnSwapHero -= HeroManager_OnSwapHero;
        Character.OnHeathChange -= Character_OnReceiveDamage;
    }

    /// <summary>
    /// Callback when character take the damage.
    /// </summary>
    /// <param name="character"></param>
    private void Character_OnReceiveDamage(Character character)
    {
        UpdateCharacterDetail(character);
    }
    /// <summary>
    /// Callback when swap hero in the line. -> Update Detail.
    /// </summary>
    /// <param name="firstAvatar"></param>
    private void HeroManager_OnSwapHero(Hero firstAvatar)
    {
        SetHeroDetail(firstAvatar.GetDetail());
    }

    /// <summary>
    /// Update the Character's detail.
    /// </summary>
    /// <param name="character"></param>
    private void UpdateCharacterDetail(Character character)
    {
        // hero
        if (character == heroManager.firstAvatar)
        {
            if (character.isDead)
            {
                if (heroManager.heroList.Count > 2)
                    SetHeroDetail(heroManager.heroList[1].GetDetail());
            }
            else
            {
                SetHeroDetail(character.GetDetail());
            }
        }
        else // enemy
        {
            if (character.isDead)
            {
                SetEnemyDetail("");
            }
            else
            {
                SetEnemyDetail(character.GetDetail());
            }
        }
    }

    /// <summary>
    /// Set hero detail in UI.
    /// </summary>
    /// <param name="detail"></param>
    public void SetHeroDetail(string detail)
    {
        heroDetailText.text = detail;
    }
    /// <summary>
    /// Set enemy detail in UI.
    /// </summary>
    /// <param name="detail"></param>
    public void SetEnemyDetail(string detail)
    {
        enemyDetailText.text = detail;
    }
    public void Show()
    {
        PlayerPanel.SetActive(true);
    }
    public void Hide()
    {
        PlayerPanel.SetActive(false);
    }
}
