﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroManager : MonoBehaviour
{
    public delegate void HeroManagerStateChangeHandler(HeroManagerStateType stateType);
    public delegate void HeroListEventHandler(int amount);
    public delegate void SwapHeroEventHandler(Hero firstAvatar);

    public static event SwapHeroEventHandler OnSwapHero;
    public static event HeroListEventHandler OnHeroListUpdate;
    public static event HeroManagerStateChangeHandler OnUpdateState;

    public enum HeroManagerStateType { Patrol, Combat }
    public HeroManagerStateType stateType;

    public Hero firstAvatar
    {
        get
        {
            return heroList.Count > 0 ? heroList[0] : null;
        }
    }
    public Hero lastAvatar
    {
        get
        {
            return heroList.Count > 0 ? heroList[heroList.Count - 1] : null;
        }
    }
    public List<Hero> heroList = new List<Hero>();
    private float _speed;
    public float speed
    {
        get { return _speed; }
        set
        {
            _speed = (value >= 10) ? 10 : value;
        }
    }

    /// <summary>
    /// heroManager state (Implement as State Pattern)
    /// </summary>
    private HeroManagerState state;


    private void OnEnable()
    {
        Hero.OnHeadCollideEnter += Hero_OnHeadCollideEnter;
        Hero.OnHeadCollideExit += Hero_OnHeadCollideExit;
    }

    private void OnDisable()
    {
        Hero.OnHeadCollideEnter -= Hero_OnHeadCollideEnter;
        Hero.OnHeadCollideExit -= Hero_OnHeadCollideExit;
        if (state != null)
        {
            state.Exit();
        }
    }


    private void Start()
    {
        speed = 3f;
        CreateHero(new Vector2(-5,-3.5f));
        UpdateState(HeroManagerStateType.Patrol);
    }

    /// <summary>
    /// Update the hero manager state.
    /// </summary>
    /// <param name="state"></param>
    public void UpdateState(HeroManagerStateType state)
    {
        stateType = state;
        if (this.state != null)
        {
            this.state.Exit();
        }
        switch (state)
        {
            case HeroManagerStateType.Patrol:
                this.state = new PatrolState();
                break;
            case HeroManagerStateType.Combat:
                this.state = new CombatState();
                break;
        }
        this.state.Enter(this);
        if (OnUpdateState != null)
        {
            OnUpdateState(stateType);
        }
        print("UpdateState : " + state);
    }

    private void Update()
    {
        if (this.state != null)
        {
            this.state.Update();
        }
    }
    /// <summary>
    /// Receive input from player controller.
    /// </summary>
    /// <param name="input"></param>
    public void HandleInput(PlayerController.InputDirection input)
    {
        if (this.state != null)
        {
            this.state.HandleInput(input);
        }
    }

    #region SwapHero
    /// <summary>
    /// Swap the hero for leading the line.
    /// </summary>
    /// <param name="isUp">is swap up</param>
    public void SwapHero(bool isUp)
    {
        if (heroList.Count < 2)
        {
            return;
        }
        SwapPosition(isUp);
        SetRelation(isUp);
        UpdateHeroList(isUp);
        SwapMovementData(isUp);
        UpdateHeroGraphic();

        if (OnSwapHero != null)
            OnSwapHero(firstAvatar);
    }
    /// <summary>
    /// Swap heroes position in the line.
    /// </summary>
    /// <param name="isUp"></param>
    private void SwapPosition(bool isUp)
    {
        if (isUp)
        {
            Vector3 lastPos = heroList[heroList.Count - 1].transform.position;
            for (int i = heroList.Count - 1; i >= 0; i--)
            {
                int previousIndex = i - 1;
                if (previousIndex >= 0)
                {
                    heroList[i].transform.position = heroList[previousIndex].transform.position;
                }
            }
            heroList[0].transform.position = lastPos;
        }
        else
        {
            Vector3 firstPos = heroList[0].transform.position;
            for (int i = 0; i < heroList.Count; i++)
            {
                int nextIndex = i + 1;
                if (nextIndex < heroList.Count)
                {
                    heroList[i].transform.position = heroList[nextIndex].transform.position;
                }
            }
            heroList[heroList.Count - 1].transform.position = firstPos;
        }
    }

    /// <summary>
    /// Update the new relation of first and last avatar.
    /// </summary>
    /// <param name="isUp"></param>
    private void SetRelation(bool isUp)
    {
        if (isUp)
        {
            firstAvatar.parent = lastAvatar;
            firstAvatar.child.parent = null;
            firstAvatar.child = null;

            lastAvatar.child = firstAvatar;
        }
        else
        {
            lastAvatar.child = firstAvatar;
            lastAvatar.parent.child = null;
            lastAvatar.parent = null;

            firstAvatar.parent = lastAvatar;
        }
    }
    /// <summary>
    /// Update hero list.
    /// </summary>
    /// <param name="isUp"></param>
    private void UpdateHeroList(bool isUp)
    {
        if (isUp)
        {
            Hero hero = firstAvatar;
            heroList.RemoveAt(0);
            heroList.Add(hero);
        }
        else
        {
            Hero hero = lastAvatar;
            heroList.RemoveAt(heroList.Count - 1);
            heroList.Insert(0, hero);
        }
    }
    /// <summary>
    /// swap the "logs" of hero's movement.
    /// </summary>
    /// <param name="isUp"></param>
    private void SwapMovementData(bool isUp)
    {
        if (isUp)
        {
            Vector2 lastDir = heroList[heroList.Count - 1].direction;
            for (int i = heroList.Count - 1; i >= 0; i--)
            {
                int previousIndex = i - 1;
                if (previousIndex >= 0)
                {
                    heroList[i].direction = heroList[previousIndex].direction;
                    heroList[i].cmdList = heroList[previousIndex].cmdList;
                }
            }
            heroList[0].direction = lastDir;
            heroList[0].cmdList = new List<Command>();
        }
        else
        {//  
            List<Command> cmdList = heroList[0].cmdList;
            Vector2 firstDir = heroList[0].direction;
            for (int i = 0; i < heroList.Count; i++)
            {
                int nextIndex = i + 1;
                if (nextIndex < heroList.Count)
                {
                    heroList[i].direction = heroList[nextIndex].direction;
                    heroList[i].cmdList = heroList[nextIndex].cmdList;
                }
            }
            heroList[heroList.Count - 1].cmdList = cmdList;
            heroList[heroList.Count - 1].direction = firstDir;
        }
    }

    /// <summary>
    /// Update Hero's graphic.
    /// </summary>
    private void UpdateHeroGraphic()
    {
        foreach (var hero in heroList)
        {
            hero.SetGraphic();
        }
    }
    #endregion

    /// <summary>
    /// Callback first avatar hit something.
    /// </summary>
    /// <param name="observer">the hero that's hit.</param>
    /// <param name="other"></param>
    private void Hero_OnHeadCollideEnter(Hero observer, GameObject other)
    {
        if (this.state != null)
        {
            this.state.OnHeadCollideEnter(observer, other);
        }
    }

    /// <summary>
    /// Callback collider exit.
    /// </summary>
    /// <param name="observer">the hero that's hit.</param>
    /// <param name="other"></param>
    private void Hero_OnHeadCollideExit(Hero observer, GameObject other)
    {
        if (this.state != null)
        {
            this.state.OnCollideExit(observer, other);
        }
    }

    /// <summary>
    /// Create hero for player.
    /// </summary>
    /// <param name="position"></param>
    public void CreateHero(Vector3 position)
    {
        Hero hero = ObjectFactory.Instance.GetHero().GetComponent<Hero>();
        hero.transform.parent = this.transform;
        hero.direction = Vector2.up;
        hero.transform.position = position;
        hero.SetBorderColor(Color.green);
        hero.SetGraphic();

        heroList.Add(hero);

        if (OnHeroListUpdate != null)
        {
            OnHeroListUpdate(heroList.Count);
        }

        if (OnSwapHero != null)
            OnSwapHero(hero);
    }

    /// <summary>
    /// Add the hero to the line.
    /// </summary>
    /// <param name="hero"></param>
    public void AddHero(Hero hero)
    {
        hero.transform.parent = this.transform;
        if (lastAvatar == null)
        {
            RefreshList();
        }
        hero.transform.position = (Vector2)lastAvatar.transform.position + lastAvatar.direction * -1 * 1.1f;
        hero.direction = lastAvatar.direction;
        hero.SetGraphic();
        hero.SetBorderColor(Color.green);

        lastAvatar.child = hero;
        hero.parent = lastAvatar;
        heroList.Add(hero);

        if (OnHeroListUpdate != null)
        {
            OnHeroListUpdate(heroList.Count);
        }
    }

    /// <summary>
    /// Remove first avatar.
    /// </summary>
    /// <param name="isClearCmdList">if want to clear the log of child's command.</param>
    public void RemoveFirstAvatar(bool isClearCmdList = true)
    {
        if (firstAvatar.child != null)
        {
            firstAvatar.child.parent = null;
            if (isClearCmdList)
            {
                firstAvatar.child.cmdList.Clear();
            }
        }
        firstAvatar.Destroy();
        heroList.RemoveAt(0);

        if (OnHeroListUpdate != null)
        {
            OnHeroListUpdate(heroList.Count);
        }
    }
    /// <summary>
    /// Remove null in herolist.
    /// </summary>
    public void RefreshList()
    {
        heroList.RemoveAll(hero => hero == null);
    }

    /// <summary>
    /// Get Total HP.
    /// </summary>
    /// <returns></returns>
    public int GetTotalHPOfAvatars()
    {
        int sum = 0;
        foreach (Hero hero in heroList)
        {
            sum += hero.currentHp;
        }
        return sum;
    }
}