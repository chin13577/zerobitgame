﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public enum InputDirection { Left, Right, Up, Down }
    HeroManager heroManager;

    void Start()
    {
        heroManager = GetComponent<HeroManager>();
    }

    void Update()
    {
        if (GameManager.Instance.state != GameManager.GameState.GamePlay)
            return;

        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            heroManager.HandleInput(InputDirection.Left);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            heroManager.HandleInput(InputDirection.Right);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            heroManager.HandleInput(InputDirection.Up);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            heroManager.HandleInput(InputDirection.Down);
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.Instance.Pause();
        }
    }
}
