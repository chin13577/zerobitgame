﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Manage the combat state of hero manager.
/// </summary>
public class CombatState : HeroManagerState
{
    HeroManager manager;
    Character target;
    Vector2 battlePosition;
    IEnumerator moveCoroutine;
    IEnumerator checkWinCoroutine;

    public override void Enter(HeroManager heroManager)
    {
        manager = heroManager;
        Character.OnDead += Character_OnDead;
        target = manager.firstAvatar.target;
        battlePosition = manager.firstAvatar.transform.position;
        StartCombat();
    }
    private void StartCombat()
    {
        manager.firstAvatar.StartFighting();
    }

    public override void Exit()
    {
        Character.OnDead -= Character_OnDead;
        if (moveCoroutine != null)
        {
            manager.StopCoroutine(moveCoroutine);
        }
    }

    /// <summary>
    /// Callback when ever character is dead.
    /// </summary>
    /// <param name="character"></param>
    private void Character_OnDead(Character character)
    {
        //add delay for check win condition.
        if (checkWinCoroutine != null)
            manager.StopCoroutine(checkWinCoroutine);
        manager.StartCoroutine(checkWinCoroutine = CheckWinDelay(CheckWin));
    }
    private void CheckWin()
    {
        if (target.isDead)
        {
            if (manager.firstAvatar.isDead)
            {
                manager.RemoveFirstAvatar(true);
            }
            manager.speed += 1;
            manager.UpdateState(HeroManager.HeroManagerStateType.Patrol);

            // add score.
            // I change the last requirement that add score from total avatars'hp instead of the last fighter's hp.
            GameManager.Instance.scoreManager.AddScore(manager.GetTotalHPOfAvatars());
        }
        else if (manager.firstAvatar.isDead)
        {
            manager.RemoveFirstAvatar(false);
            ChangeFighterToBattle();
        }
    }
    IEnumerator CheckWinDelay(Action callback)
    {
        yield return new WaitForEndOfFrame();
        callback();
    }
    private void ChangeFighterToBattle()
    {
        if (manager.firstAvatar)
        {
            moveCoroutine = MoveToEnemy(battlePosition, () =>
           {
               manager.StopCoroutine(moveCoroutine);
               moveCoroutine = null;
               manager.firstAvatar.SetTarget(this.target);
               StartCombat();

               target.GetComponent<IInteractable>().Activate(manager.firstAvatar.gameObject);

           });
            manager.StartCoroutine(moveCoroutine);
        }
    }
    IEnumerator MoveToEnemy(Vector2 targetPos, Action callback = null)
    {
        if (manager.firstAvatar == null)
        {
            yield break;
        }
        while (true)
        {
            // if character walk more than expect position angle will return >0
            float angle = Vector2.Angle(manager.firstAvatar.direction, targetPos - (Vector2)manager.firstAvatar.transform.position);
            if (Mathf.Approximately(angle,180))
            {
                break;
            }
            manager.firstAvatar.UpdatePosition(manager.speed);
            yield return null;
        }
        if (callback != null)
        {
            callback();
        }
    }
}
