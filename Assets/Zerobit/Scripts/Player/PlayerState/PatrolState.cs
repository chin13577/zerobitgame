﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : HeroManagerState
{
    HeroManager manager;
    public override void Enter(HeroManager heroManager)
    {
        manager = heroManager;
        if (manager.firstAvatar != null)
            manager.firstAvatar.cmdList.Clear();
    }

    public override void Exit()
    {
        //
    }
    /// <summary>
    /// Receive input from player controller.
    /// </summary>
    /// <param name="input"></param>
    public override void HandleInput(PlayerController.InputDirection input)
    {
        if (manager.firstAvatar == null)
        {
            return;
        }

        Vector2 headDir = manager.firstAvatar.direction;
        switch (input)
        {
            case PlayerController.InputDirection.Left:
                headDir = headDir.Rotate(90);
                manager.firstAvatar.SetCommand(headDir);
                break;
            case PlayerController.InputDirection.Right:
                headDir = headDir.Rotate(-90);
                manager.firstAvatar.SetCommand(headDir);
                break;
            case PlayerController.InputDirection.Up:
                manager.SwapHero(true);
                break;
            case PlayerController.InputDirection.Down:
                manager.SwapHero(false);
                break;
        }
    }
    public override void Update()
    {
        if(GameManager.Instance.state!= GameManager.GameState.GamePlay)
        {
            return;
        }
        if (manager.firstAvatar)
        {
            manager.firstAvatar.UpdatePosition(manager.speed);
        }
    }
    public override void OnHeadCollideEnter(Hero observer, GameObject other)
    {
        if (other.CompareTag("Wall"))
        {
            manager.RemoveFirstAvatar();
            if (manager.firstAvatar != null)
                manager.firstAvatar.SetBestDirection();
        }
        else if (other.GetComponent<IInteractable>() != null)
        {
            if (other.CompareTag("Enemy"))
            { 
                observer.SetTarget(other.GetComponent<Character>());
                manager.UpdateState(HeroManager.HeroManagerStateType.Combat);
            }
            other.GetComponent<IInteractable>().Activate(observer.gameObject);
        }

    }
}
