﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// heroManager state. (Implement as State Pattern)
/// </summary>
public abstract class HeroManagerState
{

    public abstract void Enter(HeroManager heroManager);
    public virtual void Update()
    {
        //
    }
    public virtual void OnHeadCollideEnter(Hero observer, GameObject other)
    {
        //
    }
    public virtual void OnCollideExit(Hero observer, GameObject other)
    {
        //
    }
    public abstract void Exit();
    public virtual void HandleInput(PlayerController.InputDirection input)
    {
        //
    }
}
