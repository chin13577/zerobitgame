﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable {

    /// <summary>
    /// Activate when someone interact.
    /// </summary>
    /// <param name="sender"></param>
    void Activate(GameObject sender);
}
