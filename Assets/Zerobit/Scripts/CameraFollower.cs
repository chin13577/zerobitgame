﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{

    HeroManager heroManager;

    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero; 

    // Use this for initialization
    void Start()
    {
        heroManager = GameManager.Instance.heroManager;
    }


    private void LateUpdate()
    {
        if (heroManager.firstAvatar)
        {
            Vector3 targetPos = new Vector3(heroManager.firstAvatar.transform.position.x,
                                            heroManager.firstAvatar.transform.position.y,
                                            transform.position.z);
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, dampTime);
        }
    }
    
}
