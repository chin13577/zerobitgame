﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour, IInteractable
{
    public void Activate(GameObject sender)
    {
        if (sender.GetComponent<Character>())
        {
            GameManager.Instance.scoreManager.AddScore(10);
            Destroy(this.gameObject);
        }
    }
}
