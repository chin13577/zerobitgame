﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cherry : MonoBehaviour, IInteractable
{
    public void Activate(GameObject sender)
    {
        if (sender.GetComponent<Character>())
        {
            sender.GetComponent<Character>().IncreaseHP(10);
            Destroy(this.gameObject);
        }
    }
}
