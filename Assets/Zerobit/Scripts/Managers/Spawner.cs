﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

[Serializable]
public struct Field
{
    public Vector2 x;
    public Vector2 y;
}
public class Spawner : MonoBehaviour
{
    /// <summary>
    /// Length of field.
    /// </summary>
    public Field field;

    /// <summary>
    /// object creator.
    /// </summary>
    private ObjectFactory factory;

    private GameManager.GameState gameState;
    private HeroManager.HeroManagerStateType heroState;

    private void Awake()
    {
        factory = ObjectFactory.Instance;
    }
    /// <summary>
    /// subscribe events. 
    /// </summary>
    private void OnEnable()
    {
        HeroManager.OnUpdateState += HeroManager_OnUpdateState;
        GameManager.OnGameStateChange += GameManager_OnGameStateChange;
    }

    /// <summary>
    /// unsubscribe events.
    /// </summary>
    private void OnDisable()
    {
        HeroManager.OnUpdateState -= HeroManager_OnUpdateState;
        GameManager.OnGameStateChange -= GameManager_OnGameStateChange;
    }

    /// <summary>
    /// Callback when Hero update state.
    /// Check should spawn.
    /// </summary>
    /// <param name="stateType"></param>
    private void HeroManager_OnUpdateState(HeroManager.HeroManagerStateType stateType)
    {
        heroState = stateType;
        if (stateType == HeroManager.HeroManagerStateType.Combat)
        {
            StopAllCoroutines();
        }
        else if (stateType == HeroManager.HeroManagerStateType.Patrol)
        {
            if (gameState == GameManager.GameState.GamePlay)
            {
                StartCoroutine(SpawnObject());
            }
            else
            {
                StopAllCoroutines();
            }
        }
    }

    /// <summary>
    /// callback when game manager update state.
    /// </summary>
    /// <param name="state"></param>
    private void GameManager_OnGameStateChange(GameManager.GameState state)
    {
        gameState = state;
        if (state == GameManager.GameState.GamePlay)
        {
            if (heroState == HeroManager.HeroManagerStateType.Patrol)
            {
                StartCoroutine(SpawnObject());
            }
            else
            {
                StopAllCoroutines();
            }
        }
        else
        {
            StopAllCoroutines();
        }
    }

    /// <summary>
    /// Spawn a random object in field.
    /// </summary>
    private void SpawnRandomObjectInField()
    {
        GameObject obj = null;
        float rnd = Random.Range(0, 1f);
        if (rnd < 0.55)
        {
            obj = factory.GetHero();
        }
        else if (rnd < 0.9f)
        {
            obj = factory.GetEnemy();
        }
        else if (rnd < 0.95f)
        {
            obj = factory.GetGem();
        }
        else
        {
            obj = factory.GetPotion();
        }
        obj.transform.position = FindEmptyAreaWithRandom(field.x[0], field.x[1], field.y[0], field.y[1]);
    }

    /// <summary>
    /// Find empty area in specific range.
    /// </summary>
    /// <returns></returns>
    Vector2 FindEmptyAreaWithRandom(float minX, float maxX, float minY, float maxY)
    {
        int maxRound = 0;
        while (maxRound < 200)
        {
            Vector2 newPos = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY)); 
            if (Physics2D.OverlapBox(newPos, Vector2.one, 0) == null)
            {
                return newPos;
            }
            maxRound++;
        }
        return new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
    }
    /// <summary>
    /// Spawn object random interval.
    /// </summary>
    /// <returns></returns>
    IEnumerator SpawnObject()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(2.5f, 5f));
            for (int i = 0; i < Random.Range(1, 3); i++)
            {
                SpawnRandomObjectInField();
            }
            yield return null;

        }
    }
}
