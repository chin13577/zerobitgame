﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Game Manager. (using Singleton pattern)
/// </summary>
public class GameManager : MonoBehaviour
{
    public delegate void StateAction(GameState state);
    public static event StateAction OnGameStateChange;

    public enum GameState { Tutorial, GamePlay, End }
    public GameState state;
    public HeroManager heroManager;
    public ScoreManager scoreManager;
    public UIManager uiManager;


    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        uiManager = GetComponent<UIManager>();
    }
    private void Start()
    {
        UpdateState(GameState.Tutorial);
    }

    /// <summary>
    /// Update the Game state.
    /// </summary>
    /// <param name="state"></param>
    public void UpdateState(GameState state)
    {
        this.state = state;

        if (state == GameState.Tutorial)
        {
            Time.timeScale = 1;
            uiManager.ActiveTutorialPanel(true);
        }
        else if (state == GameState.GamePlay)
        {
            Time.timeScale = 1;
            uiManager.ActivePlayerPanel(true);
            uiManager.ActiveScorePanel(true);
        }
        else if (state == GameState.End)
        {
            Time.timeScale = 1;
            uiManager.ActiveEndingPanel(true);
        }

        if (OnGameStateChange != null)
            OnGameStateChange(state);
    }
    private void OnEnable()
    {
        HeroManager.OnHeroListUpdate += HeroManager_OnHeroListUpdate;
    }

    private void OnDisable()
    {
        HeroManager.OnHeroListUpdate -= HeroManager_OnHeroListUpdate;
    }

    /// <summary>
    /// Callback when List of Hero updated.
    /// </summary>
    /// <param name="amount"></param>
    private void HeroManager_OnHeroListUpdate(int amount)
    {
        if (amount == 0)
        {
            print("end");
            UpdateState(GameState.End);
            uiManager.ActiveEndingPanel(true); 
        }
    }
    /// <summary>
    /// pause the game.
    /// </summary>
    public void Pause()
    {
        Time.timeScale = 0;
        uiManager.ActiveMenu(true);
    }

    #region UI_Callback
    public void OnFinishTutorial()
    {
        if (state == GameState.Tutorial)
        {
            UpdateState(GameState.GamePlay);
        }
    }
    public void OnClick_ShowTutorial()
    {
        uiManager.ActiveTutorialPanel(true);
    }
    public void OnClick_Resume()
    {
        Time.timeScale = 1;
        uiManager.ActiveMenu(false);
    }
    public void OnClick_Retry()
    {
        SceneManager.LoadScene(0);
    }
    public void OnClick_Quit()
    {
        Application.Quit();
    }
    #endregion
}
