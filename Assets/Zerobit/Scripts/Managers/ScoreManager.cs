﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ScoreManager : MonoBehaviour {
    
    public int score { get; private set; }
    public Text scoreText;
    IEnumerator scoreTextCoroutine;
    public void AddScore(int amount)
    {
        score += amount;
        UpdateScoreText();
    }

    /// <summary>
    /// Update ScoreText as animate.
    /// </summary>
    private void UpdateScoreText()
    {
        if (scoreTextCoroutine != null)
            StopCoroutine(scoreTextCoroutine);
        StartCoroutine(scoreTextCoroutine = UpdateScoreTextCoroutine(score));
    }

    IEnumerator UpdateScoreTextCoroutine(int targetScore)
    {
        int scoreUiValue = Convert.ToInt32(scoreText.text);
        float duration = 1f;
        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            float progress = timer / duration;
            scoreUiValue = (int)Mathf.Lerp(scoreUiValue, targetScore, progress);
            scoreText.text = scoreUiValue.ToString();
            yield return null;
        }
        scoreUiValue = targetScore;
        scoreText.text = scoreUiValue.ToString();
        
    }

}
