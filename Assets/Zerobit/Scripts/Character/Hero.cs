﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct Command
{
    public Vector2 direction;
    public Vector2 position;

    public Command(Vector2 direction, Vector2 position)
    {
        this.direction = direction;
        this.position = position;
    }
}
public class Hero : Character, IInteractable
{
    public delegate void OnHeroCollide(Hero observer, GameObject other);
    public static event OnHeroCollide OnHeadCollideEnter;
    public static event OnHeroCollide OnHeadCollideExit;

    public Hero parent;
    public Hero child;
    /// <summary>
    /// command list that's from the parent.
    /// </summary>
    public List<Command> cmdList = new List<Command>();
    public Vector2 direction = new Vector2(0, 1);

    private HeroManager manager;

    protected override void Start()
    {
        base.Start();
        manager = GameManager.Instance.heroManager;
    }

    #region Movement
    /// <summary>
    /// Set command and Chain the command to his child.(if exist)
    /// </summary>
    /// <param name="direction"></param>
    public void SetCommand(Vector2 direction)
    {
        this.direction = direction;
        SetGraphic();
        if (child != null)
        {
            Command command = new Command(direction, transform.position);
            child.AddCommand(command);
        }
    }

    /// <summary>
    /// Add command to list.
    /// </summary>
    /// <param name="command"></param>
    public void AddCommand(Command command)
    {
        cmdList.Add(command);
    }
    /// <summary>
    /// Update graphic.
    /// </summary>
    public void SetGraphic()
    {
        if (direction.x < 0)
        {
            GetComponent<SpriteRenderer>().flipX = true; 
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false; 
        }
    }

    /// <summary>
    /// Update position and chain to the child.
    /// </summary>
    /// <param name="speed">move speed.</param>
    public void UpdatePosition(float speed)
    {
        Move(speed);
        CheckCommand();
        if (child != null)
        {
            child.UpdatePosition(speed);
        }
    }

    /// <summary>
    /// Check if command has exist. if exist do the command.
    /// </summary>
    private void CheckCommand()
    {
        if (cmdList.Count > 0)
        {
            // if this position is pass the target position.  the angle will return 180 degree.
            float angle = Vector2.Angle(direction, cmdList[0].position - (Vector2)transform.position);
            if (angle > 0)
            {
                transform.position = cmdList[0].position;
                SetCommand(cmdList[0].direction);
                
                if (parent != null && Vector2.Angle(direction, parent.transform.position - transform.position) == 0)
                {
                    transform.position = (Vector2)parent.transform.position + -1f * direction * 1.1f;
                }
                cmdList.RemoveAt(0);
            }
        }
    }

    private void Move(float speed)
    {
        transform.Translate(direction * Time.deltaTime * speed);
    }
    /// <summary>
    /// Find a safe direction to move and set to this hero.
    /// </summary>
    public void SetBestDirection()
    {
        Vector2 targetDir = this.direction;
        float maxDist = 0;
        for (int i = 0; i < 4; i++)
        {
            RaycastHit2D[] hits = new RaycastHit2D[1];
            this.GetComponent<Collider2D>().Raycast(targetDir, hits);
            if (hits[0])
            {
                if (hits[0].distance > maxDist)
                {
                    maxDist = hits[0].distance;
                    this.direction = targetDir;
                }
            }
            else
            {
                this.direction = targetDir;
                break;
            }
            targetDir = targetDir.Rotate(90);
        }
        SetCommand(this.direction);
    }
    #endregion

    #region Combat

    /// <summary>
    /// Hero's attack logic.
    /// </summary>
    /// <param name="target">enemy</param>
    protected override void Attack(Character target)
    {
        int totalAtk = (target.type == this.type) ? sword * 2 : sword;
        target.TakeDamage(totalAtk); 
    }
     
    #endregion


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (this == manager.firstAvatar)
        {
            if (OnHeadCollideEnter != null)
                OnHeadCollideEnter(this, collision.gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (this == manager.firstAvatar)
        {
            if (OnHeadCollideExit != null)
                OnHeadCollideExit(this, collision.gameObject);
        }
    }

    /// <summary>
    /// Implement IInteractable. Activate by hero in the line.
    /// </summary>
    /// <param name="sender"></param>
    public void Activate(GameObject sender)
    {
        if (manager.heroList.Contains(this) == false)
        {
            manager.AddHero(this);
        }
    }
}
