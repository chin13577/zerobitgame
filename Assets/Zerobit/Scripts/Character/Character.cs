﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public delegate void CharacterEventHandler(Character character);
    public static event CharacterEventHandler OnBirth;
    public static event CharacterEventHandler OnHeathChange;
    public static event CharacterEventHandler OnDead;

    public enum CharacterType { Red, Green, Blue }
    public CharacterType type;
    public Character target;
    public int maxHp { get; protected set; }
    public int currentHp { get; protected set; }

    public int sword { get; protected set; }
    public int shield { get; protected set; }
    public bool isDead
    {
        get { return currentHp <= 0; }
    }

    private SpriteRenderer characterSprite;
    private SpriteRenderer borderSprite;
    private SpriteRenderer typeSprite;

    protected virtual void Awake()
    {
        InitialCharacterData();
    }
    protected virtual void Start()
    {
        if (OnBirth != null)
            OnBirth(this);
    }
    private CharacterType RandomCharacterType()
    {
        return (CharacterType)Random.Range(0, System.Enum.GetNames(typeof(CharacterType)).Length);
    } 
    /// <summary>
    /// Setup character data.
    /// </summary>
    private void InitialCharacterData()
    {
        this.type = RandomCharacterType();
        SetTypeColor(this.type);
        maxHp = Random.Range(15, 20);
        currentHp = maxHp;
        sword = Random.Range(5, 10);
        shield = Random.Range(5, 10);
    }
    /// <summary>
    /// Increase hp.
    /// </summary>
    /// <param name="hp">amount</param>
    public void IncreaseHP(int hp)
    {
        currentHp += hp;
        if (currentHp > maxHp)
            currentHp = maxHp;
        if (OnHeathChange != null)
            OnHeathChange(this);
    }

    #region graphic
    /// <summary>
    /// Set sprite of character.
    /// </summary>
    /// <param name="s"></param>
    public void SetCharacterSprite(Sprite s)
    {
        characterSprite = GetComponent<SpriteRenderer>();
        characterSprite.sprite = s;
    }
    /// <summary>
    /// Set sprite type of character.
    /// </summary>
    /// <param name="type"></param>
    public void SetTypeColor(CharacterType type)
    {
        if (typeSprite == null)
        {
            typeSprite = transform.GetChild(1).GetComponent<SpriteRenderer>();
        } 
        switch (type)
        {
            case CharacterType.Red:
                typeSprite.color = Color.red;
                break;
            case CharacterType.Green:
                typeSprite.color = Color.green;
                break;
            case CharacterType.Blue:
                typeSprite.color = Color.blue;
                break;
        } 
    }
    /// <summary>
    /// Set sprite border of character.
    /// </summary>
    /// <param name="color"></param>
    public void SetBorderColor(Color color)
    {
        if (borderSprite == null)
        {
            borderSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
        }
        borderSprite.color = color;
    }
    #endregion

    #region combat
    /// <summary>
    /// Set the character's enemy.
    /// </summary>
    /// <param name="c"></param>
    public void SetTarget(Character c)
    {
        target = c;
    }
    /// <summary>
    /// Start battle.
    /// </summary>
    public virtual void StartFighting()
    {
        StartCoroutine(AttackCoroutine(target));
    }
    /// <summary>
    /// This character take damage.
    /// </summary>
    /// <param name="damage">input damage</param>
    public virtual void TakeDamage(int damage)
    {
        int totalDamage = damage - shield;
        totalDamage = totalDamage >= 1 ? totalDamage : 1;
        currentHp -= totalDamage;
        if (currentHp <= 0)
        {
            currentHp = 0;
            if (OnDead != null)
                OnDead(this);
            Destroy();
        }
        if (OnHeathChange != null)
            OnHeathChange(this);
    }
    /// <summary>
    /// Attack the target.
    /// </summary>
    /// <param name="target"></param>
    protected virtual void Attack(Character target)
    {
        int totalAtk = this.sword;
        target.TakeDamage(totalAtk);
    }
    /// <summary>
    /// Attack the enemy interval.
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    protected virtual IEnumerator AttackCoroutine(Character target)
    {
        while (true)
        { 
            Attack(target);
            if (target.isDead)
            {
                break;
            }
            yield return new WaitForSeconds(0.5f);
        }
        target = null;
    }

    #endregion
    public virtual void Destroy()
    {
        if (this != null)
            Destroy(this.gameObject);
    }

    /// <summary>
    /// Get character detail.
    /// </summary>
    /// <returns>the detail.</returns>
    public string GetDetail()
    {
        string detail = "";
        detail += "HP: " + currentHp + " / " + maxHp + "\n\n";
        detail += "Type: " + type.ToString() + "\n\n";
        detail += "Sword: " + sword + "\n\n";
        detail += "Shield: " + shield;
        return detail;
    }
}
