﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character ,IInteractable {
    
    protected override void Start()
    {
        base.Start();
    }

    /// <summary>
    /// Activate when someone interact.
    /// </summary>
    /// <param name="sender">object that interact this object</param>
    public void Activate(GameObject sender)
    {
        SetTarget(sender.GetComponent<Character>());
        StartFighting( );
    }
    
}
