﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Object Creator. make factory pattern as singleton and dont destroy for better perfomance (don't load new sprite evey game).
/// </summary>
public class ObjectFactory : MonoBehaviour
{
    [SerializeField] GameObject heroPrefab;
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] GameObject potionPrefab;
    [SerializeField] GameObject gemPrefab;
    private Sprite[] sprites;

    private static ObjectFactory _instance;
    public static ObjectFactory Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ObjectFactory>();
            }
            return _instance;
        }
    }
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        Initialize();
    }
    private void Initialize()
    {
        sprites = Resources.LoadAll<Sprite>("Sprites/Actors");
    }

    public GameObject GetHero()
    {
        GameObject charGameObject = Instantiate(heroPrefab);
        Character character = charGameObject.GetComponent<Character>();
        character.SetBorderColor(Color.white);
        character.SetCharacterSprite(sprites[Random.Range(0, sprites.Length)]);
        return charGameObject;
    }
    public GameObject GetEnemy()
    {
        GameObject charGameObject = Instantiate(enemyPrefab);
        Character character = charGameObject.GetComponent<Character>();
        character.GetComponent<Character>().SetBorderColor(Color.red);
        character.SetCharacterSprite(sprites[Random.Range(0, sprites.Length)]);
        return charGameObject;
    }
    public GameObject GetPotion()
    {
        GameObject obj = Instantiate(potionPrefab);
        return obj;
    }
    public GameObject GetGem()
    {
        GameObject obj = Instantiate(gemPrefab);
        return obj;
    }
}
